require 'rails_helper'

RSpec.describe HostnamesIp, type: :model do
  it "is valid with valid attributes" do
    FactoryBot.create(:ip)
    FactoryBot.create(:hostname)

    expect(HostnamesIp.new(hostname_id: 1, ip_id: 1)).to be_valid
  end
end
