require 'rails_helper'

RSpec.describe Ip, type: :model do
  it "is valid with valid attributes" do
    expect(Ip.new(ip: '255.255.255.255')).to be_valid
  end

  it "is not valid without a ip" do
    ip = Ip.new(ip: nil)
    expect(ip).to_not be_valid
  end

  it "is not valid with a invalid ip" do
    ip = Ip.new(ip: '255255255255')
    expect(ip).to_not be_valid
  end
end
