require 'rails_helper'

RSpec.describe Hostname, type: :model do
  it "is valid with valid attributes" do
    expect(Hostname.new(hostname: 'domain.com')).to be_valid
  end

  it "is not valid without a hostname" do
    hostname = Hostname.new(hostname: nil)
    expect(hostname).to_not be_valid
  end

  it "is not valid with a invalid hostname" do
    hostname = Hostname.new(hostname: 'domain')
    expect(hostname).to_not be_valid
  end
end
