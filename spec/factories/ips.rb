FactoryBot.define do
  factory :ip do
    ip { '255.255.255.255' }
  end
end
