FactoryBot.define do
  factory :hostname do
    hostname { 'domain.com' }
  end
end
