require "rails_helper"

RSpec.describe HostnamesIpsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/hostnames_ips").to route_to("hostnames_ips#index")
    end

    it "routes to #new" do
      expect(:get => "/hostnames_ips/new").to route_to("hostnames_ips#new")
    end

    it "routes to #show" do
      expect(:get => "/hostnames_ips/1").to route_to("hostnames_ips#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/hostnames_ips/1/edit").to route_to("hostnames_ips#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/hostnames_ips").to route_to("hostnames_ips#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/hostnames_ips/1").to route_to("hostnames_ips#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/hostnames_ips/1").to route_to("hostnames_ips#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/hostnames_ips/1").to route_to("hostnames_ips#destroy", :id => "1")
    end
  end
end
