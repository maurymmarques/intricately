class Hostname < ApplicationRecord

  has_and_belongs_to_many :ips

  validates :hostname, presence: true
  validate :valid_hostname
  validates_uniqueness_of :hostname

  def valid_hostname
    if hostname.present? && hostname.match(/^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix).present? === false
      errors.add(:hostname, 'only valid domains')
    end
  end

end
