class HostnamesIp < ApplicationRecord

  belongs_to :hostname
  belongs_to :ip

  validates_uniqueness_of :hostname, scope: :ip

end
