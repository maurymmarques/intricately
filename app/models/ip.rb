class Ip < ApplicationRecord

  has_and_belongs_to_many :hostnames

  validates :ip, presence: true
  validate :valid_ipv4
  validates_uniqueness_of :ip

  def valid_ipv4
    if ip.present? && ip.match(/\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b/).present? === false
      errors.add(:ip, 'only valid IPV4')
    end
  end

end
