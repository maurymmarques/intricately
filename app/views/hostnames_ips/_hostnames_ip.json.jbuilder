json.extract! hostnames_ip, :id, :hostname_id, :ip_id, :created_at, :updated_at
json.url hostnames_ip_url(hostnames_ip, format: :json)
