json.extract! hostname, :id, :hostname, :created_at, :updated_at
json.url hostname_url(hostname, format: :json)
