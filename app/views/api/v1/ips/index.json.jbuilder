json.array!(@ips) do |ip|
  json.ip ip.ip
  if ip.hostnames
    json.hostnames do
      json.array!(ip.hostnames) do |hostname|
        json.hostname hostname.hostname
      end
    end
  end
end