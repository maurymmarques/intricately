class HostnamesIpSerializer < ActiveModel::Serializer
  attributes :id
  has_one :hostname
  has_one :ip
end
