class HostnamesIpsController < ApplicationController
  before_action :set_hostnames_ip, only: [:show, :edit, :update, :destroy]

  # GET /hostnames_ips
  # GET /hostnames_ips.json
  def index
    @hostnames_ips = HostnamesIp.all
  end

  # GET /hostnames_ips/1
  # GET /hostnames_ips/1.json
  def show
  end

  # GET /hostnames_ips/new
  def new
    @hostnames_ip = HostnamesIp.new
  end

  # GET /hostnames_ips/1/edit
  def edit
  end

  # POST /hostnames_ips
  # POST /hostnames_ips.json
  def create
    @hostnames_ip = HostnamesIp.new(hostnames_ip_params)

    respond_to do |format|
      if @hostnames_ip.save
        format.html { redirect_to @hostnames_ip, notice: 'Hostnames ip was successfully created.' }
        format.json { render :show, status: :created, location: @hostnames_ip }
      else
        format.html { render :new }
        format.json { render json: @hostnames_ip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hostnames_ips/1
  # PATCH/PUT /hostnames_ips/1.json
  def update
    respond_to do |format|
      if @hostnames_ip.update(hostnames_ip_params)
        format.html { redirect_to @hostnames_ip, notice: 'Hostnames ip was successfully updated.' }
        format.json { render :show, status: :ok, location: @hostnames_ip }
      else
        format.html { render :edit }
        format.json { render json: @hostnames_ip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hostnames_ips/1
  # DELETE /hostnames_ips/1.json
  def destroy
    @hostnames_ip.destroy
    respond_to do |format|
      format.html { redirect_to hostnames_ips_url, notice: 'Hostnames ip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hostnames_ip
      @hostnames_ip = HostnamesIp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hostnames_ip_params
      params.require(:hostnames_ip).permit(:hostname_id, :ip_id)
    end
end
