class Api::DocsController < ActionController::Base
	# http_basic_authenticate_with name: 'api-docs', password: '2txhFsWK'
	#
	# before_action :authenticate_user!, except: [:index]
	# skip_before_filter :verify_authenticity_token

	include Swagger::Blocks

	swagger_root do
		key :swagger, '2.0'
		info do
			key :version, '1.0.0'
			key :title, 'Intricately'
			key :description, 'Intricately API Documentaion.'
			# key :termsOfService, 'http://helloreverb.com/terms/'
			contact do
				key :name, 'Maury M. Marques'
				key :email, 'maurymmarques@gmail.com'
			end
			# license do
			# 	key :name, 'MIT'
			# end
		end
		tag do
			key :name, 'DNS'
			key :description, 'DNS Operations'
			externalDocs do
				key :description, 'Find more info here'
				key :url, 'https://docs.google.com/document/d/1esrn0PTMPouEkJnOUT1AtW04TKmogZQCL_ADWcMPuJo'
			end
		end
		if Rails.env.production?
			key :host, 'https://www.intricately.com'
		else
			key :host, 'localhost:3000'
		end
		# key :basePath, '/api'
		key :consumes, ['application/json']
		key :produces, ['application/json']
		security_definition :pabx_auth do
			key :type, :basic
			key :description, 'DNS Operations'
		end
	end

	# A list of all classes that have swagger_* declarations.
	SWAGGERED_CLASSES = [
			Api::V1::DocsController,
			self,
	].freeze

	def index
		render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
	end
end