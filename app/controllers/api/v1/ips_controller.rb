module Api::V1
	class IpsController < ApiController

		# GET api/v1/dns
		def index
      page = params[:page]? params[:page].to_i : 1
      limit = 10

			@ips = Ip.includes(:hostnames).limit(limit).offset((page - 1) * limit)
		end

		# POST api/v1/dns
		def create
			hostnames_ip = JSON.parse(params.to_json)
			ip = hostnames_ip['ip']
			hostnames = hostnames_ip['hostnames']

			@ip = Ip.new(ip: ip)
			if @ip.save
				hostnames.map { |hostname|
          Hostname.new(hostname: hostname['hostname']).save
          hostname = Hostname.find_by(hostname: hostname['hostname'])
          if hostname
            HostnamesIp.new(ip_id: @ip.id, hostname_id: hostname.id).save
          end
        }
      else
				render json: { error: @ip.errors.messages }, status: :not_acceptable
			end
		end

	end
end