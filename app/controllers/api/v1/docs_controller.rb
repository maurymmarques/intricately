class Api::V1::DocsController < ApplicationController
	include Swagger::Blocks

	swagger_path '/api/v1/dns' do
		operation :post do
			key :summary, 'Creates a DNS record and its associated hostnames.'
			key :description, 'This endpoint should accept an IPv4 IP address and a list of hostnames. The response should return the ID of the DNS record created.'
			key :operationId, 'dnsPost'
			key :tags, ['DNS']
			parameter do
				key :name, :ip
				key :in, :body
				key :description, 'IPV4'
				key :required, true
				schema do
					key :'$ref', :DnsInput
				end
			end
			response 200 do
				key :description, 'DNS ID'
				schema do
					key :'$ref', :DnsResponse
				end
			end
			response 406 do
				key :description, 'Not acceptable'
				schema do
					key :'$ref', :NotAcceptable
				end
			end
		end
	end

	swagger_path '/api/v1/dns' do
		operation :get do
			key :summary, 'Returns DNS records and their hostnames'
			key :description, ''
			key :operationId, 'dnsGet'
			key :tags, ['DNS']
			parameter do
				key :name, :page
				key :in, :query
				key :description, 'Page number'
				key :required, true
			end
			response 200 do
				key :description, 'Webhooks - Subscription response'
				schema do
					key :'$ref', :DnsResponse
				end
			end
			response 404 do
				key :description, 'Not found error'
				schema do
					key :'$ref', :NotFound
				end
			end
		end
	end

	swagger_schema :DnsInput do
		property :ip do
			key :type, :string
		end
    property :hostnames do
      key :type, :array
      items do
        property :hostname do
          key :type, :string
        end
      end
    end
	end

	swagger_schema :DnsResponse do
		property :id do
			key :type, :string
		end
		property :url do
			key :type, :string
		end
	end

	swagger_schema :NotAcceptable do
		property :error do
			key :type, :string
		end
  end

  swagger_schema :NotFound do
    property :error do
      key :type, :string
    end
  end

end
