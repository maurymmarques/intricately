Rails.application.routes.draw do

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :hostnames_ips
  resources :hostnames
  resources :ips

  # API
  scope module: 'api' do
    scope '/api' do
      resources :docs, only: [:index]

      namespace :v1 do
        # resources :ips
        match '/dns', to: 'ips#index', via: :get
        match '/dns', to: 'ips#create', via: :post
      end
    end
  end
end
