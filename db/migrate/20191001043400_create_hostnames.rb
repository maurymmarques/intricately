class CreateHostnames < ActiveRecord::Migration[6.0]
  def up
    create_table :hostnames do |t|
      t.string :hostname, null: false

      t.timestamps null: false
    end
  end

  def down
    drop_table :hostnames
  end
end
