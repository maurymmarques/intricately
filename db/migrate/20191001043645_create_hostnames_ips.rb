class CreateHostnamesIps < ActiveRecord::Migration[6.0]
  def up
    create_table :hostnames_ips do |t|
      t.references :hostname, index: true, null: false
      t.references :ip, index: true, null: false

      t.timestamps null: false
    end

    add_foreign_key :hostnames_ips, :hostnames
    add_foreign_key :hostnames_ips, :ips
  end

  def down
    drop_table :hostnames_ips
  end
end
