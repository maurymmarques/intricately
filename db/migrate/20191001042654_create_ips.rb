class CreateIps < ActiveRecord::Migration[6.0]
  def up
    create_table :ips do |t|
      t.string :ip, null: false

      t.timestamps null: false
    end
  end

  def down
    drop_table :ips
  end
end
