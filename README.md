# README

## System Dependencies

* Ruby version
    2.5.1
    
* Rails version
    6.0.0     

* SQLite

    
## Installation Instructions

* Clone the project

    `git clone https://maurymmarques@bitbucket.org/maurymmarques/intricately.git`
    
* Open the project directory
    
    `cd intricately`
    
* Bundle

    `bundle install`
    
    `bundle update`
    
* Database initialization
    
    `rails db:migrate`    
    
## Project

* Run rails server (use port 3000 to work correctly with Swagger)
    
    `rails server -p 3000`

* http://localhost:3000/ips
* http://localhost:3000/hostnames

## API

* You can test and check API documentation using Swagger

http://localhost:3000/api-docs/index.html

To test the API click on button "Try it out"

## How to run the test suite

* RSpec
    
    `bundle exec rspec`